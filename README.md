# Senior Communities

## Objective

1. Identify high-risk sites specifically related to senior communities:
	* Nursing homes
	* Long-term care facilities
	* Assisted-living facities

2. Calculate localized risk-score for each community (zip code).

[Tableau Public Link](https://public.tableau.com/profile/kennylee#!/vizhome/FacilityRiskPerZIpCode/RiskScorePerZIPCodeTotalBeds?publish=yes)

[Explanation of the Risk Score](https://docs.google.com/presentation/d/1YdnjuHYuOU7xky16IkQoRjnsP80ARL8RCCZEx\_1S038/edit#slide=id.g71b12dda54\_0\_37)

Contribution by:

* El Ghali Zerhouni: egaz@mit.edu
* Alexandru Socolov: socolov@mit.edu
* Kenny Lee: kennylee@sloan.mit.edu